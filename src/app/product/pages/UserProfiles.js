import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {getProfileDetails} from "../actions/userProfileActions";
import UserProfilesDetails from "../components/UserProfilesDetails";
import {connect} from "react-redux";


class UserProfiles extends Component {

    componentDidMount() {
        this.props.getProfileDetails(this.props.match.params.id);
    }
    render() {
        const {userName, gender, description} = this.props.profileDetails;
        return (
            <div>
                <h1>User Profile</h1>
                <UserProfilesDetails userName={userName} gender={gender} description={description}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    profileDetails: state.profile.profileDetails
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    getProfileDetails
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserProfiles);
