import React from "react";

const profiles = ({userName, gender, description}) => {
    return (
        <div className="user-profiles">
            <div>
                <label>User Name:<span>{userName}</span></label>
            </div>
            <div>
                <label>Gender:<span>{gender}</span></label>
            </div>
            <div>
                <label>Description:<span>{description}</span></label>l
            </div>
        </div>
    )
};
export default profiles;
