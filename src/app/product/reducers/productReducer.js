const initState = {
    productDetails: {},
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'GET_PRODUCT_DETAILS':
            return {
                ...state,
                productDetails: action.productDetails
            };
        default:
            return state
    }
};
