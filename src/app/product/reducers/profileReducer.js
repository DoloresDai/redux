const initialState = {
    profileDetails:{}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'GET_USER_PROFILE':
            return {
                ...state,
                profileDetails: action.profileDetails,
            };
        default:
            return state;
    }
}
