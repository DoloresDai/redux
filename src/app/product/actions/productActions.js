export const getProductDetails = () => (dispatch) => {
  dispatch({
    type: 'GET_PRODUCT_DETAILS',
    productDetails: {title: 'product title', userId: 'user id', id: '123'}
  });
};
